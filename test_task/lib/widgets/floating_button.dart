import 'package:flutter/material.dart';

class CustomFloatingButton extends StatelessWidget {
  final void Function() increment;
  final Color backgroungColor;
  const CustomFloatingButton({
    Key? key,
    required this.increment,
    required this.backgroungColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: increment,
      tooltip: 'Increment',
      child: Icon(Icons.add),
      backgroundColor: backgroungColor,
    );
  }
}
